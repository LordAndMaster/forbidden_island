var GameFactory = function() {
    var Game = {};

    var islands = [
    ];
    var treasures = {
        moon: [],
        goblet: [],
        lion: [],
        fire: [],
    };

    var tiles = [];

    Game.getTiles = function() {
        return tiles;
    };

    Game.start = function() {
        var ii;

        for (ii = 0; ii < 24; ii++) {
            tiles[ii] = {
                pos: ii,
                sunk: false,
                searched: false,
                treasure: null
            };
        }

        var shuffled = _.shuffle(tiles);

        var treasure_names = [ "moon", "goblet", "lion", "fire" ];
        var name;
        for (ii = 0; ii < treasure_names.length; ii ++) {
            name = treasure_names[ii];
            treasures[name] = [
                shuffled[ii * 2].pos,
                shuffled[ii * 2 + 1].pos
            ];
            shuffled[ii * 2].treasure = name;
            shuffled[ii * 2 + 1].treasure = name;
        }
    };

    Game.searchTile = function (index) {
        if (!tiles[index]) {
            throw new Error("Invalid tile");
        }
        if (tiles[index].sunk) {
            console.log("WARN: This tile is sunk");
        }

        tiles[index].searched = true;
        return tiles[index].treasure;
    };

    Game.killTile = function (index) {
        if (!tiles[index]) {
            throw new Error("Invalid tile");
        }
        if (tiles[index].sunk) {
            console.log("WARN: This tile is sunk");
        }

        tiles[index].sunk = true;

        if (!tiles[index].treasure) {
            return;
        }

        var treasure = treasures[tiles[index].treasure];
        if (treasure[0].pos === index) {
            treasure.unshift();
        }
        else if (treasure[1].pos === index) {
            treasure.pop();
        }
        else {
            throw new Error("There should be a treasure but pos dont match");
        }

        if (!treasure.length) {
            return [ treasure, true ];
        }

        return [ treasure, false ];
    };

    return Game;
};

var BoardFactory = function (game) {

    var Board = {};

    var tiles = game.getTiles();
    var treasure_icons = {
        moon: "img/moon.png",
        goblet: "img/goblet.png",
        fire: "img/fire.png",
        lion: "img/lion.png",
    };

    var getClickFunc = function (tile_index) {
        return function() {
            var $cell = $(this);
            var tile  = tiles[tile_index];

            if (tile.searched) {
                if (tile.sunk) {
                    $cell.removeClass("sunk");
                    $cell.html('');
                } else {
                    $cell.addClass("sunk");
                    if (tile.treasure) {
                        $cell.html('<img src="img/cross.png"/>');
                    }
                }
                tile.sunk = !tile.sunk;
                return;
            }

            var treasure = game.searchTile(tile_index);
            $cell.addClass("flipped");
            if (treasure) {
                $cell.css("backgroundImage",
                          "url(" + treasure_icons[treasure] + ")");
            }
        };
    };

    Board.render = function() {
        var pi, px, py;
        var $table = $('<table></table>');
        var $cell = null;
        var $row = null;
        var tile_index = -1;
        for (pi = 0; pi < 36; pi++) {
            px = pi % 6;
            py = Math.floor(pi / 6);

            if (px === 0) {
                if ($row) {
                    $table.append($row);
                }
                $row = $('<tr></tr>');
            }
            $cell = $('<td></td>');

            if (
                ([0,5].indexOf(py) !== -1 && [0,1,4,5].indexOf(px) !== -1) ||
                ([1,4].indexOf(py) !== -1 && [0,5].indexOf(px) !== -1)
            ) {
                $cell.addClass("sea");
            } else {
                tile_index ++;
                $cell.addClass("land");
                $cell.on("click", getClickFunc(tile_index));
            }
            $row.append($cell);
        }
        $table.append($row);

        $('body').append($table);
        return $table;
    };

    return Board;
};

$(function() {
    var game  = GameFactory();
    var board = BoardFactory(game);

    board.render();
    game.start();
});
